<%
' For purposes of this demo I've deleted most superficial elements in the page, removed some classes 
' cleaned some html elements and it's the bare minimum


Function html_encode(someString) 
	'Critical that ampersand is converted first, since all entities contain them.
	html_encode = replace(replace(replace(replace(someString, "&", "&amp;"), ">", "&gt;"), "<", "&lt;"), """", "&quot;")
End Function

Function LimpaLixo(input)
	input = html_encode(input)
	input = replace(input,"'","''")
	input = replace(input,"\","\\")
	Dim lixo : lixo = array ("select ", "alter ", "execute ", "drop ", "insert ", "delete ", "xp_", "truncate ", "cast ", "/*", "*/", "@@", "U+02BC", "U+0027", ";", "`", "--")
	Dim ilixo : For ilixo = 0 to uBound(lixo)
		input = replace(input, lixo(ilixo) , "")
	Next
	LimpaLixo = input
End Function




Dim keyword : keyword = LimpaLixo(Request("keyword"))
Dim idrange : idrange = LimpaLixo(Request("idrange"))
Dim minprice : minprice = LimpaLixo(Request("minprice"))
Dim maxprice : maxprice = LimpaLixo(Request("maxprice"))
%>
<!DOCTYPE html>
<html class="no-js" lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Busca: <%=keyword%></title>
</head>
<body>

	<ul class="gprod-grid clearfix">
		<%
		ConexaoBDD 'This is the function to connect to the database'
		Err.Clear : On Error Resume Next

		Set rsF = Conexao.Execute("INSERT INTO mov_buscas (bsc_data,bsc_datetime,bsc_cartid,bsc_keywords,bsc_nofresults) VALUES "&_
			"('"&Data&"','"&Now()&"','"&CartID&"','"&keyword&idrange&"', 'regular-s') ") : Set rsF = Nothing

		If keyword<>"" Then
			sqlF=Split(Trim(keyword), " ")
			For i=0 to ubound(sqlF)
				sqlStr = sqlStr & "prod_nome LIKE '%"&sqlF(i)&"%' AND "
			Next
			sqlStr = mid(sqlStr,1,len(sqlStr)-4) 'quebra a string e recorta o final " or "
			sqlStr = "("& sqlStr &") OR ("& Replace(sqlStr, "prod_nome", "prod_texto") &") "
		End IF

		If idrange<>"" Then
			sqlF=Split(Trim(idrange), ",")
			For i=0 to ubound(sqlF)
				sqlStr = sqlStr & "prod_id = '"&sqlF(i)&"' OR "
			Next
			sqlStr = mid(sqlStr,1,len(sqlStr)-4) 'quebra a string e recorta o final " or "
		End IF
		
		On Error GoTo 0

		cWhere = "WHERE prod_status='1' "
		If sqlStr<>"" Then cWhere = cWhere & "and (" & sqlStr & ")"
		If minprice<>"" Then cWhere = cWhere & "and prod_preco >= '"&minprice&"'"
		If maxprice<>"" Then cWhere = cWhere & "and prod_preco <= '"&maxprice&"'"
		
		If(len(Request("pag"))=0)then
			pagAtual=1
		Else
			pagAtual=cInt(Request("pag"))
		End If
		rPagina = 50
		
		nRegistros = cInt( fCount("cad_produtos",""&cWhere) )
			nPaginas = cInt(nRegistros / rPagina)
			If (nRegistros / rPagina) > cInt(nRegistros / rPagina) Then nPaginas=(nPaginas + 1)

		cOrder = "ORDER BY prod_nome ASC"

		Select Case Session("sort")
			Case "1"
				cOrder = "ORDER BY prod_preco ASC"
			Case "2"
				cOrder = "ORDER BY prod_preco DESC"
			Case "3"
				cOrder = "ORDER BY prod_id DESC"
		End Select
		
		sqlStr = "SELECT prod_id,prod_data,prod_ref,prod_nome,prod_categoria,prod_precoantigo,prod_preco,marc_nome,g1.pfot_arquivo FROM cad_produtos p "&_
				"LEFT JOIN cad_grupositens ON prod_id=grpi_prodid "&_
				"LEFT JOIN cad_marcas ON prod_marca = marc_id "&_
				"LEFT JOIN cad_categorias ON prod_categoria = cat_id "&_
				"INNER JOIN cad_prodfotos g1 ON p.prod_id = g1.pfot_prodid "&_
				"LEFT OUTER JOIN cad_prodfotos g2 ON p.prod_id = g2.pfot_prodid AND g1.pfot_id < g2.pfot_id "
		sqlStr = sqlStr & cWhere &" and g2.pfot_id IS NULL GROUP BY prod_id "&cOrder&" LIMIT "&(pagAtual-1)*rPagina&","&rPagina
		Set rsF1 = Conexao.Execute(sqlStr)

		If rsF1.EOF Then Response.Write("<h3>Nenhum produto encontrado na categoria selecionada.</h3>")
		While Not rsF1.EOF
		%>
		<li>
			<a href="produto/<%=rsF1("prod_id")%>">
				<div>
					<img src="img_prod/p_<%=rsF1("pfot_arquivo")%>" alt="<%=rsF1("prod_nome")%>" class="scale-with-grid" onload="imgLoaded(this)">
				</div>
				<div class="gprod-info clearfix">
					<p class="gprod-nome"><%=rsF1("prod_nome")%></p>
					<p class="gprod-preco">R$<%=FormatNumber(rsF1("prod_preco")/100, 2)%></p>
				</div>
			</a>
			<%If fData(rsF1("prod_data"))>=fData((Now-35)) Then Response.Write(" <span class='new'></span> ")%>
			<%If rsF1("prod_precoantigo")>rsF1("prod_preco") Then Response.Write(" <span class='sale'></span> ")%>
		</li>
		<%rsF1.MoveNext
		Wend : Set rsF1=Nothing
		cConexaoBDD 'This function closes the connection to the database'
		%>
	</ul>
	
</body>
</html>
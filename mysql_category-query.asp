<!--#include file="dex/_i.asp"-->

<%ConexaoBDD
catid = LimpaLixo(Request.QueryString("id"))
fSelectAll1 "cad_categorias","WHERE cat_id='"&catid&"'","",""
If Not rsF1.EOF Then
	catnome = rsF1("cat_nome")
	catparent = rsF1("cat_parent")
End If
Set rsF1=Nothing

grpid = LimpaLixo(Request.QueryString("grupo"))
If grpid<>"" Then
	fSelectAll1 "cad_grupos","WHERE grp_id='"&grpid&"'","",""
	If Not rsF1.EOF Then
		grpnome = rsF1("grp_nome")
	End If
	Set rsF1=Nothing
End If
cConexaoBDD%>
<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
	<title>Compre seu Livro | <%=catnome%> <%If grpnome<>"" Then Response.Write(" | "&grpnome)%></title>
	
	<link rel="stylesheet" href="css/base.css?<%=cssversion%>">
	<link rel="stylesheet" href="css/layout.css?<%=cssversion%>">
	
	<script src="js/libs/modernizr-2.5.3.min.js"></script>
	<link rel="shortcut icon" href="favicon.ico">
</head>

<body>

<div class="main-content " id="categories">

	<aside class="grid_4 nav">
		<ul>
		<%ConexaoBDD
		sqlF = "prod_categoria='"&catid&"' or "

		If grpid="" Then
			fSelectAll1 "cad_categorias","WHERE cat_status='1' and cat_parent='0'","",""
		Else
			fSelectAll1 "cad_categorias","WHERE cat_status='1' and cat_parent='0'","",""
		End If

		If catid="180" Then fSelectAll1 "cad_categorias","WHERE cat_status='1' and cat_parent='0' and cat_id='180'","",""

		While Not rsF1.EOF
			Response.Write("<li>"&(rsF1("cat_nome"))&"")

			fSelectAll2 "cad_categorias","WHERE cat_parent='"&rsF1("cat_id")&"' and cat_status='1'","ORDER BY cat_nome ASC",""
			If Not rsF2.EOF Then
				Response.Write("<ul>")
				While Not rsF2.EOF
					If cstr(rsF2("cat_parent")) = cstr(catid) Then
						sqlF = sqlF & "prod_categoria='"&rsF2("cat_id")&"' or "
					End If
					Response.Write("<li class='nextlevel'><a href='categoria/"&rsF2("cat_id"))
					If grpid<>"" Then Response.Write("/escola/"&grpid)
					Response.Write("'>"&(rsF2("cat_nome"))&"</a> ")
					
					fSelectAll3 "cad_categorias","WHERE cat_parent='"&rsF2("cat_id")&"' and cat_status='1'","ORDER BY cat_nome ASC",""
					If Not rsF3.EOF Then
						Response.Write("<ul>")
						While Not rsF3.EOF
							If cstr(rsF2("cat_parent")) = cstr(catid) or cstr(rsF3("cat_parent")) = cstr(catid) Then
								sqlF = sqlF & "prod_categoria='"&rsF3("cat_id")&"' or "
							End If
							Response.Write("<li><a href='categoria/"&rsF3("cat_id"))
							If grpid<>"" Then Response.Write("/escola/"&grpid)
							Response.Write("'>"&(rsF3("cat_nome"))&"</a></li> ")

						rsF3.MoveNext
						Wend
						Response.Write("</ul>")
					End If
					Set rsF3=Nothing
				
					Response.Write("</li> ")
				rsF2.MoveNext
				Wend
				Response.Write("</ul>")
			End If
			Set rsF2=Nothing
			
			Response.Write("</li> ")
		rsF1.MoveNext
		Wend
		Set rsF1=Nothing
		sqlF = mid(sqlF,1,len(sqlF)-4) 'quebra a string e recorta o final " or " %>
		</ul>				
	</aside>
	
	<div class="grid_12">
		<span class="breadcrumbs">
			<a href="<%=Domain%>">Home</a> &raquo; 
			<a href="categoria<%If grpid<>"" Then Response.Write("//escola/"&grpid)%>">Categorias</a> &raquo; 
			<a href="categoria/<%=catid%><%If grpid<>"" Then Response.Write("/escola/"&grpid)%>"><%=catnome%></a>

			<%
			If Limpa("sort",0)<>"" Then Session("sort")=Limpa("sort",0)
			%>
			<form method="post" action="categoria/<%=catid%><%If grpid<>"" Then Response.Write("/escola/"&grpid)%>" class="flr">
				<select name="sort">
					<option value="0">Ordem: padrão</option>
					<option value="1" <%If Session("sort")="1" Then Response.Write("selected")%> >Ordem: preços menores</option>
					<option value="2" <%If Session("sort")="2" Then Response.Write("selected")%> >Ordem: preços maiores</option>
					<option value="3" <%If Session("sort")="3" Then Response.Write("selected")%> >Ordem: novidades primeiro</option>
				</select>
				<button type="submit">Ok</button>
			</form>
		</span>
		
		<%
		cWhere = "WHERE prod_status='1' "
		Select Case grpid
			Case ""
			Case Else
				cWhere = cWhere & "and grpi_grpid='"&grpid&"' "
		End Select
		
		Select Case catid
			Case ""
				cWhere = cWhere & "and prod_categoria<>''"
				cGroup = "GROUP BY prod_id ORDER BY RAND()"
				cOrder = "ORDER BY RAND()"
			Case "all"
				cWhere = cWhere & "and prod_categoria<>''"
				cGroup = "GROUP BY prod_id ORDER BY prod_nome ASC"
				cOrder = "ORDER BY prod_nome ASC"
			Case "newstuff"
				cWhere = cWhere & "and prod_categoria<>''"
				cGroup = "GROUP BY prod_id ORDER BY prod_id DESC"
				cOrder = "ORDER BY prod_id DESC"
			Case "sale"
				cWhere = cWhere & "and prod_precoantigo>prod_preco"
				cGroup = "GROUP BY prod_id ORDER BY prod_id DESC"
				cOrder = "ORDER BY prod_id DESC"
			Case "encomenda"
				cWhere = cWhere & "and ("&sqlF&")"
				cGroup = "GROUP BY prod_id ORDER BY prod_nome ASC"
				cOrder = "ORDER BY prod_nome ASC"
			Case Else
				cWhere = cWhere & "and prod_categoria<>'encomenda' and ("&sqlF&")"
				cGroup = "GROUP BY prod_id ORDER BY prod_nome ASC"
				cOrder = "ORDER BY prod_nome ASC"
		End Select
		
		If(len(Request("pag"))=0)then
			pagAtual=1
		Else
			pagAtual=cInt(Request("pag"))
		End If
		rPagina = 15
		
		If grpid = "" Then
			nRegistros = cInt( fCount("cad_produtos",""&cWhere) )
		Else
			nRegistros = cInt( fCount("cad_produtos LEFT JOIN cad_grupositens ON prod_id=grpi_prodid",""&cWhere) )
			cOrder = "ORDER BY cat_parent ASC"
		End If
			nPaginas = cInt(nRegistros / rPagina)
			If (nRegistros / rPagina) > cInt(nRegistros / rPagina) Then nPaginas=(nPaginas + 1)

		Select Case Session("sort")
			Case "1"
				cOrder = "ORDER BY prod_preco ASC"
			Case "2"
				cOrder = "ORDER BY prod_preco DESC"
			Case "3"
				cOrder = "ORDER BY prod_id DESC"
		End Select		
		
		sqlStr = "SELECT prod_id,prod_ref,prod_nome,prod_categoria,prod_precoantigo,prod_preco,marc_nome,g1.pfot_arquivo FROM cad_produtos p "&_
				"LEFT JOIN cad_grupositens ON prod_id=grpi_prodid "&_
				"LEFT JOIN cad_marcas ON prod_marca = marc_id "&_
				"LEFT JOIN cad_categorias ON prod_categoria = cat_id "&_
				"INNER JOIN cad_prodfotos g1 ON p.prod_id = g1.pfot_prodid "&_
				"LEFT OUTER JOIN cad_prodfotos g2 ON p.prod_id = g2.pfot_prodid AND g1.pfot_id < g2.pfot_id "
		sqlStr = sqlStr & cWhere &" and g2.pfot_id IS NULL GROUP BY prod_id "&cOrder&" LIMIT "&(pagAtual-1)*rPagina&","&rPagina
		Set rsF1 = Conexao.Execute(sqlStr)
		If rsF1.EOF Then Response.Write("<h3>Nenhum produto encontrado na categoria selecionada.</h3>")
		%>

		<ul class="prod-grid squares clearfix">
			<%While Not rsF1.EOF%>
			<li>
				<a href="produto/<%=rsF1("prod_id")%>" class="grid_3 alpha"><img src="img_prod/p_<%=rsF1("pfot_arquivo")%>" class="scale-with-grid"></a>
				<div class="grid_9 omega prod-info">
					<div>
						<h3><%=rsF1("prod_nome")%></h3>
						<p><%=rsF1("marc_nome")%></p>
					</div>
					<div>
						<%If rsF1("prod_precoantigo")>rsF1("prod_preco") Then%><strike class="de">De: R$ <%=FormatNumber(rsF1("prod_precoantigo")/100, 2)%></strike> <%End If%>
						Por: <br> R$ <span class="por"> <%=FormatNumber(rsF1("prod_preco")/100, 2)%></span> <br>
						<%If rsF1("prod_preco")>3000 Then%><span class="em">ou 3x de R$<%=FormatNumber(rsF1("prod_preco")/3/100, 2)%> sem juros</span><%End If%>
					</div>
					<a href="produto/<%=rsF1("prod_id")%>" class="button">COMPRAR</a>
				</div>
			</li>
			<%rsF1.MoveNext
			Wend : Set rsF1=Nothing
			cConexaoBDD%>
		</ul>

		<small class="pagination clearfix">
		<%For i=1 to nPaginas%>
			<a href="categoria/<%=catid%><%If grpid<>"" Then Response.Write("/escola/"&grpid)%>/pag/<%=i%>" class="btn"><%=i%></a>
		<%Next
		Response.Write("<br class='clear'> Página "&pagAtual&" de "&nPaginas&" | Total: "&nRegistros&" Registros")%>
		</small>
	</div>
	
</div>

</body>
</html>

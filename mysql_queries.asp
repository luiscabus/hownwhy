<%
' MySQL queries 
' Inner Join vs Outer Join - I'll try to demonstrate how and why I used these and others in my projects



' >> Inner Join
' Products holds name, price, category ... (and so on). Subproducts holds product_id, name, stock ...
' To list the active subproducts of a specific category, sort by name and have some info about the product it's related to

"SELECT psub_prodid,psub_nome,psub_estoque,prod_id,prod_nome,prod_preco,prod_categoria FROM cad_produtos INNER JOIN cad_prodsubtipos ON cad_prodsubtipos.psub_prodid=cad_produtos.prod_id WHERE prod_categoria='"&cat_id&"' prod_status='1' ORDER BY psub_nome ASC"

' It will not return subproducts that are not related to products, and will not return products that doesn't have subproducts, it's like an intersection
' I could write this query in different ways: 
' cad_prodsubtipos INNER JOIN cad_produtos // cad_prodsubtipos LEFT JOIN cad_produtos (since by design, no prosub_id can be created without a product)





' >> Left Join
' I take events items and complete their information with corresponding event's info and category ... In this query I don't care what order
' the images come (pfot_id)
"SELECT * FROM cad_eventositens LEFT JOIN cad_produtos ON cad_eventositens.evei_prodid = cad_produtos.prod_id LEFT JOIN cad_categorias ON cad_produtos.prod_categoria = cad_categorias.cat_id LEFT JOIN (SELECT * FROM cad_prodfotos ORDER BY pfot_id DESC) AS temp ON cad_produtos.prod_id=temp.pfot_prodid WHERE evei_eveid='"&eveid&"' GROUP BY prod_id ORDER BY prod_nome ASC"



' When I care about what order the pictures come, I use: (if I want to make it better, see search-page.asp for reference)
' sqlF is a complement, that indicated what categories ID I want to use as filter
"SELET * FROM cad_produtos LEFT JOIN (SELECT * FROM cad_prodfotos ORDER BY pfot_id DESC) AS temp ON temp.pfot_prodid = cad_produtos.prod_id WHERE prod_status='1' and pfot_arquivo<>'' and prod_categoria<>'0' and ("&sqlF&") GROUP BY prod_id ORDER BY prod_nome ASC"






' For more complex code, please check 				search-page.asp 		and 			category_query.asp


%>